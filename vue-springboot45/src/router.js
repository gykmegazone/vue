import Vue from "vue";
import Router from "vue-router";
import ItemList from "./components/ItemList.vue";
import AddMember from "./components/AddMember.vue";
import Member from "./components/Member.vue";
import ProductOrder from "./components/ProductOrder";
import Coupon from "./components/Coupon.vue";
import CouponList from "./components/CouponList.vue";
import SearchOrder from "./components/SearchOrder"
import SearchOrderList from "./components/SearchOrderList"

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [

        // 회원 정보 조회 
        {
            path: "/membersupdate",
            name: "member-update",
            component: Member
        },

        // 회원 가입
        {
            path: "/add",
            name: "add",
            component: AddMember
        },

        // 상품 목록
        {
            path: "/itemlist",
            name: "itemlist",
            component: ItemList
        },

        // 상품 주문
        {
            path: "/order",
            name: "order",
            component: ProductOrder
        },

        // 쿠폰 조회
        {
            path: "/coupon",
            name: "coupon",
            alias: "/couponlist",
            component: Coupon,
            children: [{
                path: "couponlist/:id",
                name: "coupon-list",
                component: CouponList,
                props: true
            }]
        },

        // 주문 조회
        {
            path: "/searchorder",
            name: "searchorder",
            alias: "/searchorderlist",
            component: SearchOrder,
            children: [{
                path: "searchorderlist/:id",
                name: "searchorder-list",
                component: SearchOrderList,
                props: true
            }]
        },

    ]
});